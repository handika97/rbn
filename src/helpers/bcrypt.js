const bcrypt = require("bcryptjs");

let hashPassword = (password) => {
  let salt = bcrypt.genSaltSync(10);
  let hash = bcrypt.hashSync(password, salt);
  password = hash;
  return password;
};

let comparePass = (inputPass, password) => {
  let isValid = bcrypt.compareSync(inputPass, password);
  return isValid;
};

module.exports = { hashPassword, comparePass };
