const jwt = require("jsonwebtoken");
const secret = process.env.PRIVATE_KEY;
const { response } = require("./response");
const { cek_id } = require("../models/kyc");

const generateToken = (data) => {
  const token = jwt.sign(
    {
      email: data.email,
      date: new Date(),
    },
    secret
  );
  return token;
};

const verifyToken = (req, res, next) => {
  const token = req.headers["token"];
  cek_id(token)
    .then((result) => {
      if (result.length > 0) {
        try {
          var decoded = jwt.verify(token, process.env.PRIVATE_KEY);
          next();
        } catch (err) {
          response(res, 401, null, "Error", "Token Invalid");
        }
      } else {
        response(res, 401, null, "Error", "Token Invalid");
      }
    })
    .catch(() => {});
};

module.exports = { generateToken, verifyToken };
