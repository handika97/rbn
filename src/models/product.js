const db = require("../config/db");
const { hashPassword } = require("../helpers/bcrypt");

module.exports = {
  createProduct: (params, image_url) => {
    return new Promise((resolve, reject) => {
      let queries = `INSERT INTO product(id_user, name, description, price, category,photo,status, publish) VALUES (?,?,?,?,?,?,"ready",1)`;
      let values = [
        params.id_user,
        params.name,
        params.description,
        params.price,
        params.category,
        image_url,
      ];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(err.sqlMessage);
        }
      });
    });
  },
  gettAllProduct: (params) => {
    return new Promise((resolve, reject) => {
      let queries = `SELECT a.*, c.name category_name, b.name seller_name, b.store_name store_name, b.phone seller_phone, b.email seller_email, b.photo seller_photo, b.address seller_address, b.longitude, b.latitude FROM product a inner join kyc b on a.id_user=b.id inner join category c on a.category=c.id`;
      let values = [];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(err.sqlMessage);
        }
      });
    });
  },
  getProductByCategory: (params) => {
    return new Promise((resolve, reject) => {
      let queries = `SELECT a.*, c.name category_name, b.name seller_name, b.store_name store_name, b.phone seller_phone, b.email seller_email, b.photo seller_photo, b.address seller_address, b.longitude, b.latitude FROM product a inner join kyc b on a.id_user=b.id inner join category c on a.category=c.id where a.category=?`;
      let values = [params.id_category];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(err.sqlMessage);
        }
      });
    });
  },
  getProductBySearch: (params) => {
    return new Promise((resolve, reject) => {
      let queries = `SELECT a.*, c.name category_name, b.name seller_name, b.store_name store_name, b.phone seller_phone, b.email seller_email, b.photo seller_photo, b.address seller_address, b.longitude, b.latitude FROM product a inner join kyc b on a.id_user=b.id inner join category c on a.category=c.id where a.name like ?`;
      let values = ["%" + params.search + "%"];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(err.sqlMessage);
        }
      });
    });
  },
  getMyProductBySearch: (params) => {
    return new Promise((resolve, reject) => {
      let queries = `SELECT a.*, c.name category_name, b.name seller_name, b.store_name store_name, b.phone seller_phone, b.email seller_email, b.photo seller_photo, b.address seller_address, b.longitude, b.latitude FROM product a inner join kyc b on a.id_user=b.id inner join category c on a.category=c.id where a.name like ? and a.id_user=?`;
      let values = ["%" + params.search + "%", params.id_user];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(err.sqlMessage);
        }
      });
    });
  },
  getAllMyProduct: (params) => {
    return new Promise((resolve, reject) => {
      let queries = `SELECT a.*, c.name category_name, b.name seller_name, b.store_name store_name, b.phone seller_phone, b.email seller_email, b.photo seller_photo, b.address seller_address, b.longitude, b.latitude FROM product a inner join kyc b on a.id_user=b.id inner join category c on a.category=c.id where a.id_user=?`;
      let values = [params.id_user];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(err.sqlMessage);
        }
      });
    });
  },
  getMyProductByCategory: (params) => {
    return new Promise((resolve, reject) => {
      console.log(params);
      let queries = `SELECT a.*, c.name category_name, b.name seller_name, b.store_name store_name, b.phone seller_phone, b.email seller_email, b.photo seller_photo, b.address seller_address, b.longitude, b.latitude FROM product a inner join kyc b on a.id_user=b.id inner join category c on a.category=c.id where a.id_user=? and a.category=?`;
      let values = [params.id_user, params.id_category];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(err.sqlMessage);
        }
      });
    });
  },
  getDetailProduct: (params) => {
    return new Promise((resolve, reject) => {
      let queries = `SELECT a.*, c.name category_name, b.name seller_name, b.store_name store_name, b.phone seller_phone, b.email seller_email, b.photo seller_photo, b.address seller_address, b.longitude, b.latitude FROM product a inner join kyc b on a.id_user=b.id inner join category c on a.category=c.id where a.id=? limit 1`;
      let values = [params.id];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(err.sqlMessage);
        }
      });
    });
  },
  updateProduct: (params, Image_url) => {
    return new Promise((resolve, reject) => {
      let queries = `UPDATE product SET name=?, description=?, price=?, category=?, status=? WHERE id= ?`;
      let values = [
        params.name,
        params.description,
        params.price,
        params.category,
        params.status,
        params.id,
      ];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(new Error(err));
        }
      });
    });
  },
  updatePhotoProduct: (params, Image_url) => {
    return new Promise((resolve, reject) => {
      let queries = `UPDATE product SET photo=? WHERE id= ?`;
      let values = [Image_url, params.id];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(new Error(err));
        }
      });
    });
  },
  updateStatusProduct: (params) => {
    return new Promise((resolve, reject) => {
      let queries = `UPDATE product SET status=? WHERE id= ?`;
      let values = [params.status, params.id];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(new Error(err));
        }
      });
    });
  },
  deleteProduct: (params) => {
    return new Promise((resolve, reject) => {
      let queries = `DELETE FROM product WHERE id= ?`;
      let values = [params.id];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(err.sqlMessage);
        }
      });
    });
  },
  logout: (params) => {
    return new Promise((resolve, reject) => {
      let queries = `UPDATE employee SET token_user=? WHERE id=?`;
      let values = [null, params];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(new Error(err));
        }
      });
    });
  },
  delete: (params) => {
    return new Promise((resolve, reject) => {
      let queries = `DELETE FROM product WHERE id=?`;
      let values = [params.id];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(new Error(err));
        }
      });
    });
  },
};
