const db = require("../config/db");
const { hashPassword } = require("../helpers/bcrypt");

module.exports = {
  register: (params) => {
    return new Promise((resolve, reject) => {
      const password = hashPassword(params.password);
      let queries = `INSERT INTO kyc(name, store_name, phone, email, password, address, role) VALUES (?,?,?,?,?,?,?)`;
      let values = [
        params.name.toLowerCase(),
        params.store_name ? params.store_name.toLowerCase() : null,
        parseInt(params.phone),
        params.email.toLowerCase(),
        password,
        params.address,
        params.role,
      ];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(err.sqlMessage);
        }
      });
    });
  },
  reset: (params) => {
    return new Promise((resolve, reject) => {
      const password = hashPassword(params.password);
      let queries = `UPDATE kyc SET password = ? WHERE id = ?`;
      let values = [password, params.id];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(err.sqlMessage);
        }
      });
    });
  },
  login: (params) => {
    return new Promise((resolve, reject) => {
      let queries = `SELECT * FROM kyc WHERE email = ? LIMIT 1`;
      let values = [params.email.toLowerCase()];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(err.sqlMessage);
        }
      });
    });
  },
  uploadPhoto: (params, image_url) => {
    return new Promise((resolve, reject) => {
      let queries = `UPDATE kyc SET longitude=?, latitude=?, photo=? WHERE id= ?`;
      let values = [params.longitude, params.latitude, image_url, params.id];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(err.sqlMessage);
        }
      });
    });
  },
  updateLocation: (params, image_url) => {
    return new Promise((resolve, reject) => {
      let queries = `UPDATE kyc SET longitude=?, latitude=?, photo=? WHERE id= ?`;
      let values = [params.longitude, params.latitude, image_url, params.id];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(err.sqlMessage);
        }
      });
    });
  },
  update: (params) => {
    return new Promise((resolve, reject) => {
      let queries = `UPDATE kyc SET name=?, store_name=?, phone=?, address=? WHERE id= ?`;
      let values = [
        params.name.toLowerCase(),
        params.store_name ? params.store_name.toLowerCase() : null,
        parseInt(params.phone),
        params.address,
        params.id,
      ];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(new Error(err));
        }
      });
    });
  },
  token: (token, id) => {
    return new Promise((resolve, reject) => {
      let queries = `UPDATE kyc SET token=? WHERE id= ?`;
      let values = [token, id];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(new Error(err));
        }
      });
    });
  },
  logout: (params) => {
    return new Promise((resolve, reject) => {
      let queries = `UPDATE employee SET token_user=? WHERE id=?`;
      let values = [null, params];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(new Error(err));
        }
      });
    });
  },
  getAllUser: (params) => {
    return new Promise((resolve, reject) => {
      let queries = `SELECT * FROM kyc WHERE role=?`;
      let values = [params.role];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(err.sqlMessage);
        }
      });
    });
  },
  getUserBySearch: (params) => {
    return new Promise((resolve, reject) => {
      let queries = `SELECT * FROM kyc WHERE role=? AND name LIKE ?`;
      let values = [params.role, "%" + params.search + "%"];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(err.sqlMessage);
        }
      });
    });
  },
  getUserById: (params) => {
    return new Promise((resolve, reject) => {
      let queries = `SELECT * FROM kyc WHERE id= ?`;
      let values = [params.id];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(err.sqlMessage);
        }
      });
    });
  },
  delete: (params) => {
    return new Promise((resolve, reject) => {
      let queries = `DELETE FROM kyc WHERE id= ?`;
      let values = [params.id];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(err.sqlMessage);
        }
      });
    });
  },
};
