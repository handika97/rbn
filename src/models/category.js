const db = require("../config/db");
const { hashPassword } = require("../helpers/bcrypt");

module.exports = {
  create: (params, image_url) => {
    return new Promise((resolve, reject) => {
      let queries = `INSERT INTO category(name) VALUES (?)`;
      let values = [params.name];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(err.sqlMessage);
        }
      });
    });
  },
  getCategory: (params, image_url) => {
    return new Promise((resolve, reject) => {
      let queries = `SELECT * from category`;
      let values = [params.name];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(err.sqlMessage);
        }
      });
    });
  },
  updateCategory: (params, image_url) => {
    return new Promise((resolve, reject) => {
      let queries = `UPDATE category set name = ? WHERE id = ?`;
      let values = [params.name, params.id];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(err.sqlMessage);
        }
      });
    });
  },
  deleteCategory: (params, image_url) => {
    return new Promise((resolve, reject) => {
      let queries = `DELETE FROM category WHERE id = ?`;
      let values = [params.id];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(err.sqlMessage);
        }
      });
    });
  },
};
