const express = require("express");
const multer = require("multer");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./upload");
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + "-" + file.originalname);
  },
});
const upload = multer({ storage });

const kycRouter = express.Router();
const Controller = require("../controllers/kyc");
const { verifyToken } = require("../helpers/jwt");

kycRouter
  .post("/register", Controller.register)
  .post("/login", Controller.login)
  .get("/:role/:search", Controller.getAllUser)
  .post("/uploadphoto", upload.single("Image"), Controller.uploadPhoto)
  .patch("/update", Controller.update)
  .patch("/reset", Controller.reset)
  .delete("/:id", Controller.delete)
  .patch("/logout", Controller.logout);

module.exports = kycRouter;
