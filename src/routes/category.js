const express = require("express");
const categoryRouter = express.Router();
const Controller = require("../controllers/category");
const { verifyToken } = require("../helpers/jwt");

categoryRouter
  .post("/create", Controller.create)
  .get("/", Controller.getCategory)
  .patch("/", Controller.updateCategory)
  .delete("/:id", Controller.deleteCategory);

module.exports = categoryRouter;
