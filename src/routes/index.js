const express = require("express");
const Router = express.Router();
const kyc = require("./kyc");
const product = require("./product");
const category = require("./category");
const { verifyToken } = require("../helpers/jwt");
const Controller = require("../controllers/kyc");

Router.use("/kyc", kyc).use("/product", product).use("/category", category);

module.exports = Router;
