const express = require("express");
const multer = require("multer");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./upload");
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + "-" + file.originalname);
  },
});
const upload = multer({ storage });

const productRouter = express.Router();
const Controller = require("../controllers/product");
const { verifyToken } = require("../helpers/jwt");

productRouter
  .post("/create", upload.single("Image"), Controller.createProduct)
  .get("/:id_category/:search/:page", Controller.getProduct)
  .get("/owner/:id_category/:id_user/:search", Controller.getMyProduct)
  .get("/:id", Controller.getDetailProduct)
  .patch("/update", Controller.updateProduct)
  .patch("/updatephoto", upload.single("Image"), Controller.updatePhotoProduct)
  .patch("/status", Controller.updateStatusProduct)
  .delete("/:id", Controller.deleteProduct);

module.exports = productRouter;
