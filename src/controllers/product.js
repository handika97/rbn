const Product = require("../models/product");
const { generateToken } = require("../helpers/jwt");
const { comparePass } = require("../helpers/bcrypt");
const { response } = require("../helpers/response");
require("dotenv").config();
class ProductController {
  static createProduct(req, res) {
    if (
      req.body.id_user &&
      req.body.name &&
      req.body.description &&
      req.body.price &&
      req.body.category
    ) {
      Product.createProduct(req.body, process.env.BASE_URL + req.file.filename)
        .then((data) => {
          if (data.affectedRows === 1) {
            response(res, 201, null, "Success");
          } else {
            response(res, 400, null, "Error", "gagal membuat product baru");
          }
        })
        .catch((err) => {
          response(res, 400, null, "Error", err);
        });
    } else {
      response(res, 400, null, "Error", "Body Tidak Lengkap");
    }
  }
  static getProduct(req, res) {
    console.log(req.params);
    if (req.params.id_category === "all" && req.params.search === "null") {
      Product.gettAllProduct(req.params)
        .then((data) => {
          console.log(data);
          response(res, 200, data, "Success");
        })
        .catch((err) => {
          console.log(err);
          response(res, 400, null, "Error", err);
        });
    } else if (
      req.params.search === "null" &&
      req.params.id_category != "all"
    ) {
      Product.getProductByCategory(req.params)
        .then((data) => {
          response(res, 200, data, "Success");
        })
        .catch((err) => {
          response(res, 400, null, "Error", err);
        });
    } else {
      Product.getProductBySearch(req.params)
        .then((data) => {
          response(res, 200, data, "Success");
        })
        .catch((err) => {
          response(res, 400, null, "Error", err);
        });
    }
  }
  static getMyProduct(req, res) {
    console.log(req.params);
    if (req.params.id_category === "all" && req.params.search === "null") {
      Product.getAllMyProduct(req.params)
        .then((data) => {
          response(res, 200, data, "Success");
        })
        .catch((err) => {
          console.log("err");
          response(res, 400, null, "Error", err);
        });
    } else if (
      req.params.search === "null" &&
      req.params.id_category != "all"
    ) {
      Product.getMyProductByCategory(req.params)
        .then((data) => {
          response(res, 200, data, "Success");
        })
        .catch((err) => {
          console.log("err");
          response(res, 400, null, "Error", err);
        });
    } else {
      Product.getMyProductBySearch(req.params)
        .then((data) => {
          response(res, 200, data, "Success");
        })
        .catch((err) => {
          console.log("err");
          response(res, 400, null, "Error", err);
        });
    }
  }
  static getDetailProduct(req, res) {
    console.log(req.params);
    console.log("yee");
    Product.getDetailProduct(req.params)
      .then((data) => {
        console.log(data);
        response(res, 200, { ...data[0] }, "Success");
      })
      .catch((err) => {
        response(res, 400, null, "Error", err);
      });
  }
  static updateProduct(req, res) {
    if (
      req.body.id &&
      req.body.name &&
      req.body.description &&
      req.body.price &&
      req.body.status &&
      req.body.category
    ) {
      Product.updateProduct(req.body)
        .then((data) => {
          if (data.affectedRows === 1) {
            Product.getDetailProduct(req.body)
              .then((data1) => {
                console.log(data1);
                response(res, 200, { ...data1[0] }, "Success");
              })
              .catch((err) => {
                console.log("yee1");
                response(res, 400, null, "Error", err);
              });
          } else {
            response(res, 400, null, "Error", "gagal update product baru");
          }
        })
        .catch((err) => {
          console.log("yee2");

          response(res, 400, null, "Error", err);
        });
    } else {
      response(res, 400, null, "Error", "Body Tidak Lengkap");
    }
  }
  static updatePhotoProduct(req, res) {
    console.log(req.file);
    if (req.body.id) {
      Product.updatePhotoProduct(
        req.body,
        process.env.BASE_URL + req.file.filename
      )
        .then((data) => {
          if (data.affectedRows === 1) {
            Product.getDetailProduct(req.body)
              .then((data1) => {
                response(res, 200, { ...data1[0] }, "Success");
              })
              .catch((err) => {
                console.log("err2");

                response(res, 400, null, "Error", err);
              });
          } else {
            response(res, 400, null, "Error", "gagal update product baru");
          }
        })
        .catch((err) => {
          console.log("err1");
          response(res, 400, null, "Error", err);
        });
    } else {
      response(res, 400, null, "Error", "Body Tidak Lengkap");
    }
  }
  static updateStatusProduct(req, res) {
    Product.updateStatusProduct(req.body)
      .then((data) => {
        response(res, 201, null, "Success");
      })
      .catch((err) => {
        response(res, 400, null, "Error", err);
      });
  }
  static deleteProduct(req, res) {
    console.log(req.params);
    Product.deleteProduct(req.params)
      .then((data) => {
        if (data.affectedRows === 1) {
          response(res, 201, null, "Success");
        } else {
          response(res, 400, null, "Error", "Product Tidak Di Temukan");
        }
      })
      .catch((err) => {
        response(res, 400, null, "Error", err);
      });
  }
}

module.exports = ProductController;
