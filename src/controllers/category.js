const Category = require("../models/category");
const { generateToken } = require("../helpers/jwt");
const { comparePass } = require("../helpers/bcrypt");
const { response } = require("../helpers/response");
require("dotenv").config();
class CategoryController {
  static create(req, res) {
    if (req.body.name) {
      Category.create(req.body)
        .then((data) => {
          if (data.affectedRows === 1) {
            response(res, 201, null, "Success");
          } else {
            response(res, 400, null, "Error", "gagal membuat category baru");
          }
        })
        .catch((err) => {
          response(res, 400, null, "Error", err);
        });
    } else {
      response(res, 400, null, "Error", "Body Tidak Lengkap");
    }
  }
  static getCategory(req, res) {
    Category.getCategory(req.params)
      .then((data) => {
        console.log(data);
        response(res, 200, data, "Success");
      })
      .catch((err) => {
        response(res, 400, null, "Error", err);
      });
  }
  static updateCategory(req, res) {
    console.log(req.body);
    Category.updateCategory(req.body)
      .then((data) => {
        response(res, 200, data, "Success");
      })
      .catch((err) => {
        response(res, 400, null, "Error", err);
      });
  }
  static deleteCategory(req, res) {
    Category.deleteCategory(req.params)
      .then((data) => {
        console.log(data);
        response(res, 200, data, "Success");
      })
      .catch((err) => {
        response(res, 400, null, "Error", err);
      });
  }
}

module.exports = CategoryController;
