const KYC = require("../models/kyc");
const { generateToken } = require("../helpers/jwt");
const { comparePass } = require("../helpers/bcrypt");
const { response } = require("../helpers/response");
require("dotenv").config();
const port = process.env.SERVER_PORT || 5000;
class KYCController {
  static register(req, res) {
    if (
      req.body.name &&
      req.body.phone &&
      req.body.email &&
      req.body.password &&
      req.body.address
    ) {
      if (req.body.role === 2) {
        if (!req.body.store_name) {
          return response(res, 400, null, "Error", "Body Tidak Lengkap");
        }
      }
      let regex = new RegExp(`^62`);
      if (!regex.test(req.body.phone)) {
        req.body.phone = "62" + req.body.phone.slice(1, req.body.phone.length);
      }
      console.log(parseInt(req.body.phone));
      KYC.register(req.body)
        .then((data) => {
          if (data.affectedRows === 1) {
            response(res, 201, null, "Success");
          } else {
            response(res, 400, null, "Error", "gagal register");
          }
        })
        .catch((err) => {
          console.log(err);
          response(res, 400, null, "Error", err);
        });
    } else {
      console.log("err");
      response(res, 400, null, "Error", "Body Tidak Lengkap");
    }
  }
  static login(req, res) {
    KYC.login(req.body)
      .then((data) => {
        if (data[0]) {
          if (comparePass(req.body.password, data[0].password)) {
            const token = generateToken(data[0]);
            KYC.token(token, data[0].id).then((result) => {
              response(
                res,
                200,
                {
                  ...data[0],
                  token: token,
                },
                "Success"
              );
            });
          } else {
            response(res, 400, null, "Error", "Password Salah");
          }
        } else {
          response(res, 400, null, "Error", "Belum Terdaftar");
        }
      })
      .catch((err) => {
        response(res, 400, null, "Error", "Belum Terdaftar");
      });
  }
  static uploadPhoto(req, res) {
    console.log(req.file.filename, req.body);
    if (req.file.filename && req.body.id) {
      KYC.uploadPhoto(req.body, process.env.BASE_URL + req.file.filename)
        .then((data) => {
          KYC.getUserById(req.body)
            .then((data1) => {
              console.log(data1);
              response(res, 200, data1[0], "Success");
            })
            .catch((err) => {
              response(res, 400, null, "Error");
            });
        })
        .catch((err) => {
          response(res, 400, null, "Error", "Upload Foto Gagal");
        });
    } else {
      response(res, 400, null, "Error", "Body Tidak Lengkap");
    }
  }

  static update(req, res) {
    console.log(req.body);
    if (req.body.name && req.body.phone && req.body.id && req.body.address) {
      let regex = new RegExp(`^62`);
      if (!regex.test(req.body.phone)) {
        req.body.phone = "62" + req.body.phone.slice(1, req.body.phone.length);
      }
      KYC.update(req.body)
        .then((data) => {
          KYC.getUserById(req.body)
            .then((data1) => {
              console.log(data1);
              response(res, 201, data1[0], "Success");
            })
            .catch((err) => {
              response(res, 400, null, "Error");
            });
        })
        .catch((err) => {
          response(res, 400, null, "Error");
        });
    } else {
      response(res, 400, null, "Error", "Body Tidak Lengkap");
    }
  }
  static logout(req, res) {
    KYC.logout(req.headers["token"])
      .then((data) => {
        response(res, 201, null, "Success");
      })
      .catch((err) => {
        response(res, 400, null, "Error");
      });
  }
  static getAllUser(req, res) {
    console.log(req.params);
    if (req.params.search === "null") {
      KYC.getAllUser(req.params)
        .then((data) => {
          response(res, 201, data, "Success");
        })
        .catch((err) => {
          console.log(err);
          response(res, 400, null, "Error", err);
        });
    } else {
      KYC.getUserBySearch(req.params)
        .then((data) => {
          response(res, 201, data, "Success");
        })
        .catch((err) => {
          console.log(err);
          response(res, 400, null, "Error", err);
        });
    }
  }
  static delete(req, res) {
    KYC.delete(req.params)
      .then((data) => {
        response(res, 201, data, "Success");
      })
      .catch((err) => {
        console.log(err);
        response(res, 400, null, "Error", err);
      });
  }

  static reset(req, res) {
    if (req.body.password) {
      KYC.reset(req.body)
        .then((data) => {
          response(res, 201, data, "Success");
        })
        .catch((err) => {
          console.log(err);
          response(res, 400, null, "Error", err);
        });
    } else {
      response(res, 400, null, "Error");
    }
  }
}

module.exports = KYCController;
